const graphql = require('graphql');
const User = require('../db/user.js');
const Comment = require('../db/comment.js');
const Post = require('../db/post.js');
const Hobby = require('../db/hobby.js');
const Room = require('../db/room.js');
const Chat = require('../db/chat.js');
const Privateroom = require('../db/privateroom.js');
const Friend = require('../db/friend.js');
var GraphQLDate = require('graphql-date')

const { 
  GraphQLObjectType,
  GraphQLSchema,
  GraphQLID,
  GraphQLString, 
  GraphQLInt, 
  GraphQLBoolean,
  GraphQLList,
  GraphQLNonNull
} = graphql;
  
const UserType = new GraphQLObjectType({
  name: 'User',
  fields: () => ({
    id: { type: GraphQLString },
    name: { type: new GraphQLNonNull(GraphQLString) },
    profileImage: { type: new GraphQLNonNull(GraphQLString) },
      everyRelatedPost: {
          type: new GraphQLList(PostType),
          resolve(parent, args) {
              return Post.find({ idUser: parent.id });
          }
      },
    everyRelatedComment: {
      type: new GraphQLList(CommentType),
      resolve(parent, args) {
return Comment.find({ idUser: parent.id });
      }
    },
    everyRelatedRoom: {
      type: new GraphQLList(RoomType),
      resolve(parent, args) {
return Room.find({ creator: parent.id });
      }
    },
    everyRelatedPrivateRoom: {
      type: new GraphQLList(PrivateroomType),
      resolve(parent, args) {
// return Privateroom.find({ idUser: parent.id });
// return Privateroom.find( {idUser: parent.id});
return Privateroom.find( {"clientId": { "$regex": parent.id, "$options": "i" }});

      }
    },
    everyRelatedFriend: {
      type: new GraphQLList(FriendType),
      resolve(parent, args) {
return Friend.find({ idUser: parent.id });
      }
    },
  })
});

const CommentType = new GraphQLObjectType({
  name: 'Comment',
  fields: () => ({
    id: { type: GraphQLString },
    content: { type: GraphQLString },
    idUser: { type: GraphQLString },
    relatedUser: {
      type: UserType,
      resolve(parent, args) {
return User.findById(parent.idUser);
      }
    },
    idPost: { type: GraphQLString },
    relatedPost: {
      type: PostType,
      resolve(parent, args) {
return Post.findById(parent.idPost);
      }
    }
  })
});

const PostType = new GraphQLObjectType({
  name: 'Post',
  fields: () => ({
    id: { type: GraphQLString },
    idUser: { type: GraphQLString },
    title: { type: GraphQLString },
    content: { type: GraphQLString },
    everyRelatedComment: {
      type: new GraphQLList(CommentType),
      resolve(parent, args) {
return Comment.find({ idPost: parent.id });
      }
    },
    relatedUser: {
      type: UserType,
      resolve(parent, args) {
return User.findById(parent.idUser);
      }
    }
    
//      everyRelatedRoom: {
//       type: new GraphQLList(RoomType),
//       resolve(parent, args) {
// return Room.find({ creator: parent.id });
//       }
//     },
    
    
  })
});



const HobbyType = new GraphQLObjectType({
  name: 'Hobby',
  fields: () => ({
    id: { type: GraphQLString },
    name: { type: GraphQLString }
  })
});

const RoomType = new GraphQLObjectType({
  name: 'Room',
  fields: () => ({
    id: { type: GraphQLString },
    creator: { type: GraphQLString },
    name: { type: GraphQLString },
    quantity: { type: GraphQLInt },
    client: { type: new GraphQLList(GraphQLString) },
    everyLatestChat: {
      type: new GraphQLList(ChatType),
      resolve(parent, args) {
return Chat.find({ roomId: parent.id }).sort({x:-1}).limit(1);      
      }
    },
    everyRelatedChat: {
      type: new GraphQLList(ChatType),
      resolve(parent, args) {
return Chat.find({ roomId: parent.id });      
      }
    },
    relatedUser: {
      type: UserType,
      resolve(parent, args) {
return User.findById(parent.creator);
      }
    }
  })
});

const PrivateroomType = new GraphQLObjectType({
  name: 'Privateroom',
  fields: () => ({
    id: { type: GraphQLString },
    idUser: {type: GraphQLString},
    roomId: { type: GraphQLString },
    client: { type: new GraphQLList(GraphQLString) },
    clientId: { type: new GraphQLList(GraphQLString) },
    limit: { type: GraphQLInt },
    quantity: { type: GraphQLInt },
    name: { type: GraphQLString },
    everyLatestChat: {
      type: new GraphQLList(ChatType),
      resolve(parent, args) {
return Chat.find({ roomId: parent.roomId }).sort({x:-1}).limit(1);      
      }
    }, 
    everyRelatedChat: {
      type: new GraphQLList(ChatType),
      resolve(parent, args) {
return Chat.find({ roomId: parent.roomId });
      }
    },
  })
});

const ChatType = new GraphQLObjectType({
  name: 'Chat',
  fields: () => ({
    id: { type: GraphQLString },
    roomId: { type: GraphQLString },
    idUser: { type: GraphQLString },
    date: {type: GraphQLDate },
    content: { type: GraphQLString },
    relatedRoom: {
      type: PrivateroomType,
      resolve(parent, args) {
return Privateroom.findById(parent.roomId);
      }
    },
      relatedUser: {
      type: UserType,
      resolve(parent, args) {
return User.findById(parent.idUser);
      }
    }
  })
});

const FriendType = new GraphQLObjectType({
  name: 'Friend',
  fields: () => ({
    id: { type: GraphQLString },
    idUser: { type: GraphQLString },
    relatedUser: {
      type: UserType,
      resolve(parent, args) {
return User.findById(parent.idUser);
      }
    }
  })
});













const RootQuery = new GraphQLObjectType({
  name: 'RootQueryType',
  fields: {
    everyUser: {
      type: new GraphQLList(UserType),
      resolve() {
        return User.find({});
      }
    },
    user: {
      type: UserType,
      args: { id: { type: GraphQLString}},
      resolve(parent, args) {
        return User.findById(args.id);
      }
    },
    everyComment: {
      type: new GraphQLList(CommentType),
      resolve() {
        return Comment.find({});
      }
    },
    comment: {
      type: CommentType,
      args: { id: { type: GraphQLString}},
      resolve(parent, args) {
        return Comment.findById(args.id);
      }
    },
    everyPost: {
      type: new GraphQLList(PostType),
      resolve() {
        return Post.find({});
      }
    },
    post: {
      type: PostType,
      args: { id: { type: GraphQLString}},
      resolve(parent, args) {
        return Post.findById(args.id);
      }
    },
    everyChat: {
      type: new GraphQLList(ChatType),
            args: { id: { type: GraphQLString}},
      resolve(parent, args) {
        return Chat.find( {"roomId": { "$regex": args.id, "$options": "i" }}).sort({"date": 1});
      }
    },
    chat: {
      type: ChatType,
      args: { id: { type: GraphQLString}},
      resolve(parent, args) {
                return Chat.findById(args.id);
      }
    },
    everyHobby: {
      type: new GraphQLList(HobbyType),
      resolve() {
        return Hobby.find({});
      }
    },
    hobby: {
      type: HobbyType,
      args: { id: { type: GraphQLString}},
      resolve(parent, args) {
        return Hobby.findById(args.id);
      }
    },
    everyRoom: {
      type: new GraphQLList(RoomType),
      resolve() {
        return Room.find({});
      }
    },
    room: {
      type: RoomType,
      args: { id: { type: GraphQLString}},
      resolve(parent, args) {
        return Room.findById(args.id);
      }
    },
    everyPrivateroom: {
      type: new GraphQLList(PrivateroomType),
      resolve() {
        return Privateroom.find({});
      }
    },
    privateroom: {
      type: PrivateroomType,
      args: { id: { type: GraphQLString}},
      resolve(parent, args) {
        return Privateroom.findById(args.id);
      }
    },
      privateroomuser: {
      type: new GraphQLList(PrivateroomType),
            args: { id: { type: GraphQLString}},
      resolve(parent, args) {
        return Privateroom.find( {"clientId": { "$regex": args.id, "$options": "i" }});
      }
    },
    everyFriend: {
      type: new GraphQLList(FriendType),
      resolve() {
        return Friend.find({});
      }
    },
    friend: {
      type: FriendType,
      args: { id: { type: GraphQLString}},
      resolve(parent, args) {
        return Friend.findById(args.id);
      }
    }
  }
});

const Mutation = new GraphQLObjectType({
  name: 'Mutation',
  fields: {
    addUser: {
      type: UserType,
      args: {
        id: { type: GraphQLString },
        name: { type: new GraphQLNonNull(GraphQLString)},
        password: { type: new GraphQLNonNull(GraphQLString)}
      },
      resolve(parent, args) {
        const user = new User(args);
        return user.save();
      }
    },
    updateUser: {
      type: UserType,
      args: {
        id: { type: GraphQLString },
        name: { type: new GraphQLNonNull(GraphQLString) }
      },
      resolve(parent, args) {
        return User.findByIdAndUpdate(args.id, args);
      }
    },
    deleteUser: {
      type: UserType,
      args: { id: { type: GraphQLString}},
      resolve(parent, args) {
        return User.findByIdAndRemove(args.id);
      }
    },
    addComment: {
      type: CommentType,
      args: {
        id: { type: GraphQLString },
        idUser: { type: GraphQLString },
        idPost: { type: GraphQLString },
        content: { type: GraphQLString }
      },
      resolve(parent, args) {
        const comment = new Comment(args);
        return comment.save();
      }
    },
    updateComment: {
      type: CommentType,
      args: {
        id: { type: GraphQLString },
        idUser: { type: GraphQLString },
        idPost: { type: GraphQLString }
      },
      resolve(parent, args) {
        return Comment.findByIdAndUpdate(args.id, args);
      }
    },
    deleteComment: {
      type: CommentType,
      args: { id: { type: GraphQLString}},
      resolve(parent, args) {
        return Comment.findByIdAndRemove(args.id);
      }
    },
        addChat: {
      type: ChatType,
      args: {
        id: { type: GraphQLString },
        roomId: { type: GraphQLString },
        content: { type: GraphQLString },
        idUser: { type: GraphQLString }
      },
      resolve(parent, args) {
        const chat = new Chat(args);
        return chat.save();
      }
    },
    updateChat: {
      type: ChatType,
      args: {
        id: { type: GraphQLString },
        roomId: { type: GraphQLString },
        content: { type: GraphQLString },
        idUser: { type: GraphQLString }
      },
      resolve(parent, args) {
        return Chat.findByIdAndUpdate(args.id, args);
      }
    },
    deleteChat: {
      type: ChatType,
      args: { id: { type: GraphQLString}},
      resolve(parent, args) {
        return Chat.findByIdAndRemove(args.id);
      }
    },
    addPost: {
      type: PostType,
      args: {
        id: { type: GraphQLString },
        title: { type: GraphQLString },
        content: { type: GraphQLString },
        idUser: { type: GraphQLString }
      },
      resolve(parent, args) {
        const post = new Post(args);
        return post.save();
      }
    },
    updatePost: {
      type: PostType,
      args: {
        id: { type: GraphQLString },
        title: { type: GraphQLString },
        content: { type: GraphQLString },
        idUser: { type: GraphQLString }
      },
      resolve(parent, args) {
        return Post.findByIdAndUpdate(args.id, args);
      }
    },
    deletePost: {
      type: PostType,
      args: { id: { type: GraphQLString}},
      resolve(parent, args) {
        return Post.findByIdAndRemove(args.id);
      }
    },
    addHobby: {
      type: HobbyType,
      args: {
        id: { type: GraphQLString },
        name: { type: GraphQLString }
      },
      resolve(parent, args) {
        const hobby = new Hobby(args);
        return hobby.save();
      }
    },
    updateHobby: {
      type: HobbyType,
      args: {
        id: { type: GraphQLString },
        name: { type: GraphQLString }
      },
      resolve(parent, args) {
        return Hobby.findByIdAndUpdate(args.id, args);
      }
    },
    deleteHobby: {
      type: HobbyType,
      args: { id: { type: GraphQLString}},
      resolve(parent, args) {
        return Hobby.findByIdAndRemove(args.id);
      }
    },
    addRoom: {
      type: RoomType,
      args: {
        id: { type: GraphQLString },
        creator: { type: GraphQLString },
        client: { type: new GraphQLList(GraphQLString) },
        name: { type: GraphQLString },
        quantity: { type: GraphQLInt },
      },
      resolve(parent, args) {
        const room = new Room(args);
        return room.save();
      }
    },
    updateRoom: {
      type: RoomType,
      args: {
        id: { type: GraphQLString },
        creator: { type: GraphQLString },
        client: { type: new GraphQLList(GraphQLString) },
        name: { type: GraphQLString },
        quantity: { type: GraphQLInt },
      },
      resolve(parent, args) {
        return Room.findByIdAndUpdate(args.id, args);
      }
    },
    deleteRoom: {
      type: RoomType,
      args: { id: { type: GraphQLString}},
      resolve(parent, args) {
        return Room.findByIdAndRemove(args.id);
      }
    },
    addPrivateroom: {
      type: PrivateroomType,
      args: {
        id: { type: GraphQLString },
        roomId: { type: GraphQLString },
        client: { type: new GraphQLList(GraphQLString) },
        clientId: { type: new GraphQLList(GraphQLString) },
        limit: { type: GraphQLInt },
        quantity: { type: GraphQLInt },
        name: { type: GraphQLString }
      },
      resolve(parent, args) {
        const privateroom = new Privateroom(args);
        return privateroom.save();
      }
    },
    updatePrivateroom: {
      type: PrivateroomType,
      args: {
        id: { type: GraphQLString },
        roomId: { type: GraphQLString },
        client: { type: new GraphQLList(GraphQLString) },
        clientId: { type: new GraphQLList(GraphQLString) },
        limit: { type: GraphQLInt },
        quantity: { type: GraphQLInt },
        name: { type: GraphQLString }
      },
      resolve(parent, args) {
        return Privateroom.findByIdAndUpdate(args.id, args);
      }
    },
    deletePrivateroom: {
      type: PrivateroomType,
      args: { id: { type: GraphQLString}},
      resolve(parent, args) {
        return Privateroom.findByIdAndRemove(args.id);
      }
    },
    addFriend: {
      type: FriendType,
      args: {
        id: { type: GraphQLString },
        idUser: { type: GraphQLString }
      },
      resolve(parent, args) {
        const friend = new Friend(args);
        return friend.save();
      }
    },
    updateFriend: {
      type: FriendType,
      args: {
        id: { type: GraphQLString },
        idUser: { type: GraphQLString }
      },
      resolve(parent, args) {
        return Friend.findByIdAndUpdate(args.id, args);
      }
    },
    deleteFriend: {
      type: FriendType,
      args: { id: { type: GraphQLString}},
      resolve(parent, args) {
        return Friend.findByIdAndRemove(args.id);
      }
    }
  }
});

module.exports = new GraphQLSchema({
  query: RootQuery,
  mutation: Mutation
});
