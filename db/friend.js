const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const friendSchema = new Schema({
  idUser: {
    type: String,
    unique: false,
    required: false
  }
});

module.exports = mongoose.model("Friend", friendSchema);