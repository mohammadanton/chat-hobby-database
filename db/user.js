const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userSchema = new Schema({
  name: {
    type: String,
    unique: false,
    required: true
  },
  profileImage: {
    type: String,
    unique: false,
    required: true,
    default: "noimage"
  },
  password: {
    type: String,
    unique: false,
    required: true
  }
});

module.exports = mongoose.model("User", userSchema);