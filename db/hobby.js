const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const hobbySchema = new Schema({
  name: [{
    type: String,
    unique: false,
    required: false
  }],
});

module.exports = mongoose.model("Hobby", hobbySchema);