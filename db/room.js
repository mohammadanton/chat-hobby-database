const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const room = new Schema({
  creator: {
    type: String,
    unique: false,
    required: false
  },
  client: [{
    type: String,
    unique: false,
    required: false
  }],
  name: {
    type: String,
    unique: false,
    required: false
  },
  quantity: {
    type: Number,
    unique: false,
    required: false,
    default: "1"
  }
});

module.exports = mongoose.model("Room", room);
