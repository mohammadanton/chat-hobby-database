const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const postSchema = new Schema({
  title: {
    type: String,
    unique: false,
    required: false
  },
  content: {
    type: String,
    unique: false,
    required: false
  },
  idUser: {
    type: String,
    unique: false,
    required: false
  }
});

module.exports = mongoose.model("Post", postSchema);