const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const commentSchema = new Schema({
  idUser: {
    type: String,
    unique: false,
    required: false
  },
  idPost: {
    type: String,
    unique: false,
    required: false
  },
    content: {
        type: String,
        unique: false,
        required: false
    }
});

module.exports = mongoose.model("Comment", commentSchema);