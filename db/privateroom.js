const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const privateroomSchema = new Schema({
  roomId: {
    type: String,
    unique: false,
    required: false
  },
  client: [{
    type: String,
    unique: false,
    required: false
  }],
  clientId: [{
    type: String,
    unique: false,
    required: false
  }],
  limit: {
    type: Number,
    unique: false,
    required: false,
    default: "2"
  },
  quantity: {
    type: Number,
    unique: false,
    required: false,
    default: "1"
  },
  name: {
    type: String,
    unique: false,
    required: false
  },
  idUser: {
    type: String,
    unique: false,
    required: false
  }
});

module.exports = mongoose.model("Privateroom", privateroomSchema);
