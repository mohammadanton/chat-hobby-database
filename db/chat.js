const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ChatSchema = new Schema({
  roomId: {
    type: String,
    unique: false,
    required: false
  },
  content: {
    type: String,
    unique: false,
    required: false
  },
  idUser: {
    type: String,
    unique: false,
    required: false
  },
  date: {
		type: Date,
		default: (Date.now() + 60 * 60 * 7000),
		required: true
	},
});

module.exports = mongoose.model("Chat", ChatSchema);